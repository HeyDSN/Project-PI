<?php
  header("Cache-Control: no-cache, must-revalidate");

  if(isset($_POST['pesan'])){
    // sertakan berkas utama
    $role = "none";
    require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';
    $nama = $_POST['nama'];
    $email = $_POST['email'];
    $pesan = $_POST['pesan'];
    $query = $mysqli->prepare('INSERT INTO pesan (nama, email, pesan)values(?, ?, ?)');
    $query->bind_param('sss', $nama, $email, $pesan);
    $query->execute();
    die;
  }
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<!-- Bagian META -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Kontak - UG BEMF Events</title>
<meta name="description" content="Gunadarma awesome events info on one place."/>

<!-- Bagian STYLE -->
<link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;amp;lang=en" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/materialize/css/material.indigo-pink.min.css" rel="stylesheet">
<link href="/materialize/css/main.css" rel="stylesheet">
<link href="/dash/css/font-awesome.min.css" rel="stylesheet">

<!-- Search -->
<link rel="stylesheet" type="text/css" href="/materialize/css/default.css" />
<link rel="stylesheet" type="text/css" href="/materialize/css/component.css" />
<script src="/materialize/js/modernizr.custom.js"></script>
<!-- Search END -->
</head>

<body id="top">
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
  
  <div style="width:240px;" id="search-button">
    <div id="sb-search" class="sb-search">
      <form action="events.php" method="GET">
        <input class="sb-search-input" placeholder="Tekan (Enter)..." type="text" value="" name="q" id="search">
        <input class="sb-search-submit" type="submit" value="">
        <span class="sb-icon-search"></span>
      </form>
    </div>
  </div>

  <header class="mdl-layout__header mdl-layout__header--waterfall site-header" style="overflow:visible;">
    <div class="mdl-layout__header-row site-logo-row">
      <span class="mdl-layout__title">
        <div class="site-logo"></div>
      </span>
    </div>
    
    <div class="mdl-layout__header-row site-navigation-row mdl-layout--large-screen-only">
      <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
        <a class="mdl-navigation__link" href="./">Home</a>
      <a class="mdl-navigation__link" href="events.php">Acara</a>
      <a class="mdl-navigation__link" href="about.php">Tentang</a>
      <a class="mdl-navigation__link" href="contact.php">Kontak</a>
      <a class="mdl-navigation__link" href="/portal/">Portal</a>
      <div class="mdl-navigation__link dropdown">
          <a class="mdl-navigation__link" href="javascript:void(0);" style="padding:0px;">Login</a>
          <div class="dropdown-content">
              <a href="/admin/">Admin</a>
              <a href="/bem/">BEM</a>
              <a href="/ketuplak/">Ketuplak</a>
          </div>
      </div>
      </nav>
    </div>
  </header>

  <div class="mdl-layout__drawer mdl-layout--small-screen-only">
      <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
          <a class="mdl-navigation__link" href="./">Home</a>
          <a class="mdl-navigation__link" href="events.php">Acara</a>
          <a class="mdl-navigation__link" href="about.php">Tentang</a>
          <a class="mdl-navigation__link" href="contact.php">Kontak</a>
          <a class="mdl-navigation__link" href="/portal/">Portal</a>
          <a class="mdl-navigation__link" href="/admin/">Login Admin</a>
          <a class="mdl-navigation__link" href="/bem/">Login BEM</a>
          <a class="mdl-navigation__link" href="/ketuplak/">Login Ketuplak</a>
      </nav>
  </div>

  <main class="mdl-layout__content">
    <div class="site-content">
      <div class="mdl-grid site-max-width">
        <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--4dp page-content">
          <div class="mdl-card__title">
            <h1 class="mdl-card__title-text">Contact</h1>
          </div>
          
          <div class="mdl-card__media">
            <img class="article-image" src="/images/contact.jpg" border="0" alt="Contact">
          </div>

          <div class="mdl-grid site-copy">
            <div class="mdl-cell mdl-cell--12-col">
              <div class="mdl-card__supporting-text">
                <div id="formPesan">
                  <p>Contact our team using the contact form below:</p>
                  <form id="kirimPesan" method="POST" class="form-contact">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                      <input class="mdl-textfield__input" pattern="[A-Z,a-z, ]*" type="text" name="nama" id="nama" required>
                      <label class="mdl-textfield__label" for="nama">Nama...</label>
                      <span class="mdl-textfield__error">Letters and spaces only</span>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                      <input class="mdl-textfield__input" type="text" name="email" id="email" required>
                      <label class="mdl-textfield__label" for="email">Email...</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                      <textarea class="mdl-textfield__input" type="text" rows="5" name="pesan" id="pesan" required></textarea>
                      <label class="mdl-textfield__label" for="pesan">Pesan Anda...</label>
                    </div>
                    <p><button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" onclick="kirimPesan();">Submit</button></p>
                  </form>
                </div>
                <div id="pesanPesan" style="display:none">
                  <p align="center">
                    <i class="fa fa-check aria-hidden fa-5x"></i><br/>
                    Thank you, your message has been sent. Our team will reply your message to the email you enter as soon as possible.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <footer class="mdl-mini-footer">
      <div class="footer-container">
        <div class="mdl-logo">&copy; 2017 Kopiadem. Back to <a href="/">HOME</a></div>
      </div>
    </footer>
  </main>

  <!-- Jquery Core Js -->
  <script src="/dash/plugins/jquery/jquery.min.js"></script>

  <script src="/materialize/js/material.min.js" defer></script>
  <script src="/materialize/js/ajax.js" defer></script>

  <!-- Search -->
  <script src="/materialize/js/classie.js"></script>
  <script src="/materialize/js/uisearch.js"></script>
  <script>
  new UISearch( document.getElementById( 'sb-search' ) );
  </script>

</div>
</body>
</html>
