<?php
// Cek status login
if(empty($_SESSION['UIDAdmin'])){
    header("HTTP/1.1 404 Not Found");
    die();
}

if(isset($_POST['chpasswordAdmin'])){
		$pwLama = $_POST['oldPw'];
		$pwBaru = $_POST['newPw'];

		$query=$mysqli->prepare('SELECT hash FROM admin WHERE admin_id = ?');
		$query->bind_param('s', $adminID);
		$query->execute();

		$result = $query->get_result();
		while($row = $result->fetch_array()){
			$hash = $row['hash'];
		}
		$query->close();

		if(password_verify($pwLama, $hash)){
			// Always reset token for secutory
        	$mail_hash = "Activated";
			$password = $pwBaru;
			$options = [
			'cost' => 10
			];
			$newHash = password_hash($password, PASSWORD_BCRYPT, $options);
			$query = $mysqli->prepare('UPDATE `admin` SET hash = ?, email_hash = ? WHERE admin_id = ?');
			$query->bind_param('ss', $newHash, $mail_hash, $adminID);
			$query->execute();
			$chStatus = true;
		}else{
			$chStatus = false;
		}
	}
?>
