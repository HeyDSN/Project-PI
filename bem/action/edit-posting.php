<?php
// Cek status login
if(empty($_SESSION['UIDAdmin'])){
header("HTTP/1.1 404 Not Found");
die();
}
    if(isset($_POST['editPosting'])){
        $idPosting = $_POST['idPosting'];
        $isiPosting = $_POST['isiPosting'];

        // Edit acara
        $query = $mysqli->prepare("UPDATE posting SET isi=? WHERE posting_id = ? AND admin_id = ?");
        $query->bind_param('sss', $isiPosting, $idPosting, $adminID);
        if($query->execute()){
            $status="1";
            $pesan="Postingan berhasil diubah. <a href='/bem/posting/view/'>Kembali ke daftar posting</a>";
        }else{
            $status="2";
            $pesan="Postingan gagal diubah.";
        }
        $query->close();
    }
?>
