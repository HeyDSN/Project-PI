<?php
// sertakan berkas utama
$role = "admin";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

// Cek status login
if(empty($_SESSION['UIDAdmin'])){
    header("HTTP/1.1 404 Not Found");
    die();
}

// Get hash
$query=$mysqli->prepare('SELECT `email_hash` FROM `admin` WHERE admin_id = ?');
$query->bind_param('s', $adminID);
$query->execute();
$result=$query->get_result();
while($row = $result->fetch_array()){
    $email_hash = $row['email_hash'];
}
$query->close();

include 'hash.mail.php';

header('Location: /bem/dashboard/?regenerate=sent')
?>
