<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Reset Password | Acara Online</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/dash/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/dash/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="/dash/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/dash/css/style.css" rel="stylesheet">
</head>
<?php
if(isset($_POST['resetNow'])){
    // sertakan berkas utama
    $role = "none";
    require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';
    $adminID = $_POST['adminID'];
    $mail_hash = "Activated";
    $email_status = "1";
    $password = $_POST['password'];

    // Hash password
    $options = [
    'cost' => 10
    ];
    $hash = password_hash($password, PASSWORD_BCRYPT, $options);

    $query = $mysqli->prepare('UPDATE admin SET hash = ?, email_hash = ?, email_status = ? WHERE admin_id = ?');
    $query->bind_param('ssss', $hash, $mail_hash, $email_status, $adminID);
    if($query->execute()){
        $actStat = true;
    }else{
        $actStat = false;
    }
    $query->close();
?>
    <body class="signup-page custom-bg" style="max-width: 600px;">
    <?php if($actStat == true){ ?>
        <div class="signup-box">
            <div class="logo">
                <a href="javascript:void(0);">Reset Password</a>
                <small>Password was successfully reset</small>
            </div>
            <div class="card">
                <div class="body" style="display: block; padding: 25px 30px; background: rgba(255,255,255,.9); font: 300 18px/27px 'Open Sans', Helvetica, Arial, sans-serif; text-align: center;">
                    <p align="center">
                        <i class="fa fa-check aria-hidden fa-5x" style="color: green;"></i><br/><br/>
                        Password Anda berhasil diatur ulang, silahkan login dengan password baru Anda.<br/><br/>
                        <a href="/bem/" style="font-size: 24px;">Login</a>
                    </p>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if($actStat == false){ ?>
        <div class="signup-box">
            <div class="logo">
                <a href="#">Oops!</a>
                <small>There's nothing here.</small>
            </div>
            <div class="card">
                <div class="body" style="display: block; padding: 25px 30px; background: rgba(255,255,255,.9); font: 300 18px/27px 'Open Sans', Helvetica, Arial, sans-serif; text-align: center; color: #f00;">
                    <p>
                        <i class="fa fa-times aria-hidden fa-5x"></i><br/>
                        Whatever you were looking for doesn't currently exist at this address. Unless you were looking for this error page, in which case: Congrats! You totally found it. 
                    </p>
                </div>
            </div>
        </div>
    <?php } ?>

        <!-- Jquery Core Js -->
        <script src="/dash/plugins/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="/dash/plugins/node-waves/waves.js"></script>

        <!-- SweetAlert Plugin Js -->
        <script src="/dash/plugins/sweetalert/sweetalert.min.js"></script>
        <?php if($actStat == true){ echo 
        '<script>
            function mailAct() {
                swal("Password has been reset", "Selamat, password Anda berhasil direset!", "success");
            }
        </script>
        <script>mailAct();</script>'; 
        }; ?>
    </body>
    </html>
<?php
die; }
if(isset($_GET['id'],$_GET['token'])){
    // sertakan berkas utama
    $role = "none";
    require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

    $id = $_GET['id'];
    $token = $_GET['token'];
    // Count
    $query = $mysqli->prepare('SELECT id FROM admin WHERE admin_id = ? AND email_hash = ?');
    $query->bind_param('ss', $id, $token);
    $query->execute();

    $result=$query->get_result();
    $jumlahBaris = $result->num_rows;

    if($jumlahBaris == 1){
        $actStat = true;
    }else{
        $actStat = false;
    }

    ?>
    <body class="signup-page custom-bg" style="max-width: 600px;">
    <?php if($actStat == true){ ?>
        <div class="signup-box">
            <div class="logo">
                <a href="javascript:void(0);">Reset Password</a>
                <small>Set your new password.</small>
            </div>
            <div class="card">
                <div class="body">
                    <form id="reset" method="POST">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>
                            </div>
                        </div>
                        <!-- Nyelip -->
                        <input type="hidden" name="adminID" value="<?php echo $id; ?>">
                        <!-- Nyelip -->
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" class="form-control" name="confirm" minlength="6" placeholder="Confirm Password" required>
                            </div>
                        </div>
                        <button class="btn btn-block btn-lg bg-pink waves-effect" name="resetNow" type="submit">RESET</button>
                    </form>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if($actStat == false){ ?>
        <div class="signup-box">
            <div class="logo">
                <a href="#">Oops!</a>
                <small>There's nothing here.</small>
            </div>
            <div class="card">
                <div class="body" style="display: block; padding: 25px 30px; background: rgba(255,255,255,.9); font: 300 18px/27px 'Open Sans', Helvetica, Arial, sans-serif; text-align: center; color: #f00;">
                    <p>
                        <i class="fa fa-times aria-hidden fa-5x"></i><br/>
                        Whatever you were looking for doesn't currently exist at this address. Unless you were looking for this error page, in which case: Congrats! You totally found it. 
                    </p>
                </div>
            </div>
        </div>
    <?php } ?>
        <!-- Jquery Core Js -->
        <script src="/dash/plugins/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="/dash/plugins/node-waves/waves.js"></script>

        <!-- Validation Plugin Js -->
        <script src="/dash/plugins/jquery-validation/jquery.validate.js"></script>

        <!-- Custom Js -->
        <script src="/dash/js/reset.js"></script>
    </body>
    </html>

<?php
    die;
}
// sertakan berkas utama
$role = "none";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';
if(isset($_POST['reset'])){
    if(isset($_POST['g-recaptcha-response'])){
		$captcha=$_POST['g-recaptcha-response'];
        if(!$captcha){
        	echo '<script>alert("Please check the the captcha form!");</script>';
    	}else{
			$key = $config->api_key->googleCaptchaSecret;
			$serv = $_SERVER['REMOTE_ADDR'];
    		$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$key&response=$captcha&remoteip=$serv"), true);
    		if($response['success'] == false){
        		$_SESSION['failSuperAdmin']= $no; //adding fail every failed attempt
				echo '<script>alert("Detected as Bot!");</script>';
				echo '<script>alert("Try again!");</script>';
    		}else{
                $user = $_POST['user'];
                $query1 = $mysqli->prepare('SELECT * FROM admin WHERE username = ? OR email = ?');
                $query1->bind_param('ss', $user, $user);
                $query1->execute();
                $result1=$query1->get_result();
                $jumlahBaris1=$result1->num_rows;
                while($row = $result1->fetch_array()){
                    $adminID = $row['admin_id'];
                    $namaAdmin = $row['nama'];
                    $mailAdmin = $row['email'];
                }
                
                if($jumlahBaris1 > 0){
                    $email_hash = hash('sha512', $adminID);
                    $query2 = $mysqli->prepare('UPDATE admin SET email_hash = ? WHERE admin_id = ?');
                    $query2->bind_param('ss', $email_hash, $adminID);
                    $query2->execute();
                    $query2->close();
                    include 'reset.mail.php';
                }
                $query1->close();
                $resetStatus = true;
            }
        }
    }
}
?>
<body class="login-page custom-bg">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);"><b>Reset Password</b></a>
            <small>Gunakan username atau email pada akun untuk mendapatkan link reset password yang akan dikirim melalui email.</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST">
                    <div class="msg">Reset your password</div>
                    <?php if(isset($resetStatus)){ ?>
                    <div id="autoHideElement" class="alert bg-green" role="alert">
                        Jika username / email benar, link reset password akan dikirim ke email yang terhubung dengan akun.
                    </div>
                    <?php } ?>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="user" placeholder="Username / E-Mail" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <center><div class="g-recaptcha" data-sitekey="6Lcaig4UAAAAABUUIUMyIP5D-DyIyVlgA6JSX0CQ"></div></center>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 p-t-5">
                            <!--input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label-->
                        </div>
                        <div class="col-xs-4">
                            <a class="btn btn-block bg-pink waves-effect" href="/bem/">LOGIN</a>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" name="reset" type="submit">RESET</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="/dash/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/sign-in.js"></script>
    <script src="/dash/js/auto-hide.js"></script>

    <!-- Google Captcha API -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>
