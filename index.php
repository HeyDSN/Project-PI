<?php
header("Cache-Control: no-cache, must-revalidate");
// sertakan berkas utama
$role = "none";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';
?>
<!DOCTYPE html>
<html lang="en-US">

<head>
    <!-- Bagian META -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UG BEMF Events</title>
    <meta name="description" content="Gunadarma awesome events info on one place."/>

    <!-- Bagian STYLE -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/materialize/css/material.indigo-pink.min.css" rel="stylesheet">
    <link href="/materialize/css/main.css" rel="stylesheet">
    
    <!-- Search -->
    <link rel="stylesheet" type="text/css" href="/materialize/css/default.css" />
    <link rel="stylesheet" type="text/css" href="/materialize/css/component.css" />
    <script src="/materialize/js/modernizr.custom.js"></script>
    <!-- Search END -->
</head>

<body id="top">
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <a href="contact.php" id="contact-button" class="mdl-button mdl-button--fab mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast mdl-shadow--4dp"><i class="material-icons">mail</i></a>
        <div style="width:240px;" id="search-button">
            <div id="sb-search" class="sb-search">
                <form action="events.php" method="GET">
                    <input class="sb-search-input" placeholder="Tekan (Enter)..." type="text" value="" name="q" id="search">
                    <input class="sb-search-submit" type="submit" value="">
                    <span class="sb-icon-search"></span>
                </form>
            </div>
        </div>

        <header class="mdl-layout__header mdl-layout__header--waterfall site-header" style="overflow:visible;">
            <div class="mdl-layout__header-row site-logo-row">
                <span class="mdl-layout__title">
                    <div class="site-logo"></div>
                    <!-- <span class="site-description">Material Design Portfolio</span> -->
                </span>
            </div>
            <div class="mdl-layout__header-row site-navigation-row mdl-layout--large-screen-only">
                <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
                    <a class="mdl-navigation__link" href="./">Home</a>
                    <a class="mdl-navigation__link" href="events.php">Acara</a>
                    <a class="mdl-navigation__link" href="about.php">Tentang</a>
                    <a class="mdl-navigation__link" href="contact.php">Kontak</a>
                    <a class="mdl-navigation__link" href="/portal/">Portal</a>
                    <div class="mdl-navigation__link dropdown">
                        <a class="mdl-navigation__link" href="javascript:void(0);" style="padding:0px;">Login</a>
                        <div class="dropdown-content">
                            <a href="/admin/">Admin</a>
                            <a href="/bem/">BEM</a>
                            <a href="/ketuplak/">Ketuplak</a>
                        </div>
                    </div>
                </nav>
            </div>
        </header>

        <div class="mdl-layout__drawer mdl-layout--small-screen-only">
            <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
                <a class="mdl-navigation__link" href="./">Home</a>
                <a class="mdl-navigation__link" href="events.php">Acara</a>
                <a class="mdl-navigation__link" href="about.php">Tentang</a>
                <a class="mdl-navigation__link" href="contact.php">Kontak</a>
                <a class="mdl-navigation__link" href="/portal/">Portal</a>
                <a class="mdl-navigation__link" href="/admin/">Login Admin</a>
                <a class="mdl-navigation__link" href="/bem/">Login BEM</a>
                <a class="mdl-navigation__link" href="/ketuplak/">Login Ketuplak</a>
            </nav>
        </div>
        
        <main class="mdl-layout__content">
            <div class="site-content">
                <div class="container">
                    <div class="mdl-grid site-max-width">
                        <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--4dp welcome-card portfolio-card">
                            <div class="mdl-card__title">
                                <h2 class="mdl-card__title-text"></h2>
                            </div>
                            <div class="mdl-card__supporting-text">
                                Di sini Anda dapat melihat daftar acara apa saja yang telah, sedang, atau akan berlangsung di Universitas Gunadarma.
                            </div>
                            <div class="mdl-card__actions mdl-card--border">
                                <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect mdl-button--accent" href="events.php">
                                    Ke daftar acara
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="section--center mdl-grid site-max-width">
            <?php
                $query=$mysqli->prepare('SELECT * FROM posting ORDER BY id DESC LIMIT 3');
                $query->execute();
                $result=$query->get_result();
                while($row = $result->fetch_array()){
            ?>
                <div class="mdl-cell mdl-card mdl-shadow--4dp portfolio-card">
                    <div class="mdl-card__media">
                        <img class="article-image" src="<?php echo $row['header']; ?>" border="0" alt="">
                    </div>
                    <div class="mdl-card__title">
                        <h2 class="mdl-card__title-text"><?php echo $row['judul']; ?></h2>
                    </div>
                    <div class="mdl-card__supporting-text">
                    <?php
                        $isi = $row['isi'];
                        if (strlen($row['isi']) > 100) {
                            $isi = substr($row['isi'], 0, 100).".....";
                        } else {
                            $isi = $row['isi'];
                        }
                        echo $isi;
                    ?>
                    </div>
                    <br>
                    <div class="mdl-card__actions mdl-card--border" style="position:absolute;bottom:0;">
                        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect mdl-button--accent" href="events.php?events_id=<?php echo $row['posting_id']; ?>">Selengkapnya</a>
                    </div>
                </div>
            <?php } ?>
            </section>

            <section class="section--left mdl-grid site-max-width homepage-portfolio">
                <a class="mdl-button mdl-button--raised mdl-js-button mdl-js-ripple-effect mdl-button--accent" href="events.php">
                    Lainnya
                </a>
            </section>

            <footer class="mdl-mini-footer">
                <div class="footer-container">
                    <div class="mdl-logo">&copy; 2017 Kopiadem. Back to <a href="/">HOME</a></div>
                </div>
            </footer>
        </main>

        <script src="/materialize/js/material.min.js" defer></script>

        <!-- Search -->
        <script src="/materialize/js/classie.js"></script>
        <script src="/materialize/js/uisearch.js"></script>
        <script>
            new UISearch( document.getElementById( 'sb-search' ) );
        </script>
    </div>
</body>
</html>
