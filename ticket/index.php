<?php
// sertakan berkas utama
$role = "user";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';
$accessCode = '|code\/acces\/x\/-\/ray|';
// Cek status login
if(empty($_SESSION['UIDUser'])){
    if($enableCaptcha==false){
        include 'login.php';
    }else if($enableCaptcha==true){
        include 'login-captcha.php';
    }
die();
}

if(isset($_POST['invalidateTicket'])){
    $kode = $_POST['kode'];
	$time = date("D, d F Y | g:i:s A");
	$query=$mysqli->prepare('UPDATE peserta SET hadir = 1, waktu_hadir = ? WHERE peserta_id = ?');
	$query->bind_param('ss', $time, $kode);
	$query->execute();
    include 'ticket-finish.php';
    exit;
}

// Set page
if(!isset($_GET['x'],$_GET['y'])){
    $response_array['status'] = 3;
    header('Content-type: application/json');
    echo json_encode($response_array);
    exit;
}else{
    $act = $_GET['x'];
    $kode = $_GET['y'];
}

if($act != 'verify'){
    $response_array['action'] = $act;
    $response_array['error'] = 'unknown action';
    $response_array['status'] = 3;
    header('Content-type: application/json');
    echo json_encode($response_array);
    exit;
}else{
    $query=$mysqli->prepare("SELECT peserta.*,acara.nama_acara FROM peserta INNER JOIN acara ON peserta.acara_id=acara.acara_id WHERE peserta_id = ?");
    $query->bind_param('s', $kode);
    $query->execute();
    $result=$query->get_result();
    while($row = $result->fetch_array()){
        $noreg = $row['id'];
        $nama = $row['nama'];
        $npm = $row['npm'];
        $dob = $row['tgl_lahir'];
        $phone = $row['no_hp'];
        $kehadiran = $row['hadir'];
        $waktu_kehadiran = $row['waktu_hadir'];
        $email = $row['email'];
        $acara = $row['nama_acara'];
    }

    $jumlahRow = $result->num_rows;
    if(!isset($kehadiran)){
        $kehadiran = 0;
    }
    if($kehadiran === 0){
        $tiket = 'valid';
    }else{
        $tiket = 'invalid';
    }

    if($jumlahRow === 1){
        if($tiket==='valid'){
            include 'ticket-success.php';
        }else{
            include 'ticket-failed.php';
        }
    }else{
        include 'ticket-error.php';
    }
    exit;
}
?>
