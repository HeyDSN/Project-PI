<?php
if(isset($accessCode)){
    if($accessCode == '|code\/acces\/x\/-\/ray|'){
        // nice
    }else{
        exit;
    }
}else{
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Portal | Acara Online</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/dash/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/dash/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/dash/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="/dash/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Jquery UI Css-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Sweetalert Css -->
    <link href="/dash/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/dash/css/style.css" rel="stylesheet">
</head>

<body class="signup-page custom-bg" style="max-width: 600px;">
<div class="modal"></div>
    <div class="signup-box">
        <div class="logo">
            <a href="#">Ticket Verification System</a>
        </div>
        <div class="card">
            <div class="body">

                <div class="sukses">
                    <div class="alert bg-red" role="alert" style="text-align: center;">
                        Ticket Invalid.
                    </div>
                    <p align="center">
                        <i class="fa fa-times aria-hidden fa-5x"></i>
                    </p>
                    <p align="left" style="font-size:16pt;color:red;">Tiket ditemukan dalam database dengan data seperti di bawah ini, akan tetapi tiket telah digunakan.<br/></p>
                    <?php
                    function zerofill ($num, $zerofill = 5){
                        return str_pad($num, $zerofill, '0', STR_PAD_LEFT);
                    }
                    $noreg = zerofill($noreg);
                    ?>
                    <table style="font-size:16pt;color:red;"><tbody>
                    <tr><td>Registration Code</td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td><?php echo $noreg; ?></td></tr>
                    <tr><td>Acara</td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td><?php echo $acara; ?></td></tr>
                    <tr><td>ID</td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td><?php echo $npm; ?></td></tr>
                    <tr><td>Name</td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td><?php echo $nama; ?></td></tr>
                    <tr><td>DOB</td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td><?php echo $dob; ?></td></tr>
                    <tr><td>Phone</td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td><?php echo $phone; ?></td></tr>
                    <tr><td>Waktu Kehadiran</td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td><?php echo $waktu_kehadiran; ?></td></tr>
                    </tbody></table>
                </div>

            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Jquery UI Js -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Select Plugin Js -->
    <script src="/dash/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Validation Plugin Js -->
    <script src="/dash/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="/dash/plugins/sweetalert/sweetalert.min.js"></script>
    
    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/ajax-func.js"></script>
    <script src="/dash/js/dialogs.js"></script>
</body>

</html>
