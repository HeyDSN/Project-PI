$(function () {
    //Textare auto growth
    autosize($('textarea.auto-growth'));

    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: false,
        weekStart: 1
    });

    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY',
        clearButton: false,
        weekStart: 1,
        time: false,
        minDate : new Date()
    });

    $('.timepicker').bootstrapMaterialDatePicker({
        format: 'HH:mm',
        clearButton: false,
        date: false
    });
});


$(".toggle-btn").click(function() {
    $(".umpet").toggle("slow");
    var $this = $(this);
    $this.toggleClass("show");
    if ($this.hasClass("show")) {
        $this.text("Sembunyikan");
    } else {
        $this.text("Tampilkan");
    }
});

$(".divPost").hide();
$(".postAcara").click(function() {
    if($(this).is(":checked")) {
        $(".divPost").show("slow");
    } else {
        $(".divPost").hide("slow");
    }
});

function randomPassword(length){
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}

$(".gen-pw").click(function() {
    isi = randomPassword(12);
    $('input[name=newPassword]').focus();
    $('input[name=newPassword]').val(isi);
});
