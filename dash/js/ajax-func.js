/* ========== jQuery Loading ========*/
$(document).ajaxStart(function() {
    $('html, body').css('overflow', 'hidden'); 
    $(".div-modal").show();
});

$(document).ajaxStop(function() {
    $(".div-modal").hide();
    $('html, body').css('overflow', 'visible'); 
});

function tryAgain() {
	$("#validateNPM").show();
    $('#fillProfile').html("");
}

/* ========== AJAX CEK START ========== */
	// AJAX get acara
function getAcara(val) {
	$.ajax({
		type: "POST",
		url: "/portal/get_acara.php",
		data:'idAcara='+val,
		success: function(data){
			$("#acara").html(data);
			$("#pilihan").html('<option value="" disabled="" selected="">-- Pilih Acara --</option>');
			$('#acara').selectpicker('refresh');
			$('#pilihan').selectpicker('refresh');
		}
	});
}

	// AJAX get pilihan
function getPilihan(val) {
	$.ajax({
		type: "POST",
		url: "/portal/get_pilihan.php",
		data:'idPilihan='+val,
		success: function(data){
			$("#pilihan").html(data);
			$('#pilihan').selectpicker('refresh');
		}
	});
}

	// AJAX search acara
function getSearchAcara() {
	$('#searchAcara').on('submit',function(e){
        e.preventDefault();
		$('#hasil-cari-acara').html("Mohon tunggu, sedang mencari acara :)");
		var newAcara = $('#newAcara');
		var val = newAcara.val();
		$.ajax({
			global: false,
			type: "POST",
			url: "/portal/search_acara.php",
			data:'q='+val,
			success: function(data){
				$("#hasil-cari-acara").html(data);
			}
		});
	});
}

	// AJAX validasi NPM
function validateNPMF() {
	$('#validateNPM').on('submit',function(e){
        e.preventDefault();
		$('html, body').css('overflow', 'hidden'); 
    	$(".modal").show();
		var checkNPM = $('#checkNPM');
		var valq = checkNPM.val();
		var checkAcara = $('#checkAcara');
		var valz = checkAcara.val();
		$.ajax({
			global: false,
			type: "POST",
			url: "/portal/validate_npm.php",
			data:'q='+valq+'&z='+valz,
			success: function(data){
				$(".modal").hide();
				$("#validateNPM").hide();
    			$('html, body').css('overflow', 'visible'); 
				$("#fillProfile").html(data);
				$('#domisili').selectpicker('refresh');
			}
		});
	});
}

function dtDel(val) {
	swal({
		title: "Anda yakin?",
		text: "Acara beserta data pesertanya akan dihapus secara permanen!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya, hapus semua!",
		cancelButtonText: "Eh, jangan!",
		showLoaderOnConfirm: true,
		closeOnConfirm: false,
		closeOnCancel: false
	}, function (isConfirm) {
		if (isConfirm) {
			$.ajax({
				type: "POST",
				url: "/ketuplak/action/delete-acara.php",
				data:'id='+val,
				success: function (data) {
					if (data.status == 'success') {
						swal("Deleted!", "Data seminar yang Anda pilih berhasil dihapus.", "success");
						$('#dt-del-btn').click();
					} else {
						swal("Error!", "Terjadi galat saat Anda mengirim, mohon refresh halaman dan coba lagi", "error");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					swal("Error!", "Terjadi galat saat Anda mengirim, mohon refresh halaman dan coba lagi", "error");
				}
			});
		} else {
			swal("Cancelled", "Data seminar Anda aman :)", "error");
		}
	});
};

function psrtDel(val) {
	swal({
		title: "Anda yakin?",
		text: "Data peserta akan dihapus secara permanen!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya, hapus!",
		cancelButtonText: "Eh, jangan!",
		showLoaderOnConfirm: true,
		closeOnConfirm: false,
		closeOnCancel: false
	}, function (isConfirm) {
		if (isConfirm) {
			$.ajax({
				type: "POST",
				url: "/ketuplak/action/delete-peserta.php",
				data:'id='+val,
				success: function (data) {
					if (data.status == 'success') {
						swal("Deleted!", "Data peserta yang Anda pilih berhasil dihapus.", "success");
						$('#dt-del-btn').click();
					} else {
						swal("Error!", "Terjadi galat, mohon refresh halaman dan coba lagi", "error");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					swal("Error!", "Terjadi galat, mohon refresh halaman dan coba lagi", "error");
				}
			});
		} else {
			swal("Cancelled", "Data peserta aman :)", "error");
		}
	});
};

function postDel(val) {
	swal({
		title: "Anda yakin?",
		text: "Post yang dihapus tidak dapat dikembalikan!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya, hapus!",
		cancelButtonText: "Eh, jangan!",
		showLoaderOnConfirm: true,
		closeOnConfirm: false,
		closeOnCancel: false
	}, function (isConfirm) {
		if (isConfirm) {
			$.ajax({
				type: "POST",
				url: "/ketuplak/action/delete-posting.php",
				data:'id='+val,
				success: function (data) {
					if (data.status == 'success') {
						swal("Deleted!", "Data posting yang Anda pilih berhasil dihapus.", "success");
						$('#dt-del-btn-b').click();
					} else {
						swal("Error!", "Terjadi galat, mohon refresh halaman dan coba lagi", "error");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					swal("Error!", "Terjadi galat, mohon refresh halaman dan coba lagi", "error");
				}
			});
		} else {
			swal("Cancelled", "Data posting aman :)", "error");
		}
	});
};

function postadmDel(val) {
	swal({
		title: "Anda yakin?",
		text: "Post yang dihapus tidak dapat dikembalikan!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya, hapus!",
		cancelButtonText: "Eh, jangan!",
		showLoaderOnConfirm: true,
		closeOnConfirm: false,
		closeOnCancel: false
	}, function (isConfirm) {
		if (isConfirm) {
			$.ajax({
				type: "POST",
				url: "/bem/action/delete-posting.php",
				data:'id='+val,
				success: function (data) {
					if (data.status == 'success') {
						swal("Deleted!", "Data posting yang Anda pilih berhasil dihapus.", "success");
						$('#dt-del-btn-b').click();
					} else {
						swal("Error!", "Terjadi galat, mohon refresh halaman dan coba lagi", "error");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					swal("Error!", "Terjadi galat, mohon refresh halaman dan coba lagi", "error");
				}
			});
		} else {
			swal("Cancelled", "Data posting aman :)", "error");
		}
	});
};

function usrDel(val) {
	swal({
		title: "WARNING!",
		text: "Seluruh data acara dan peserta pada akun ini akan dihapus secara permanen, apakah Anda yakin?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya, hapus!",
		cancelButtonText: "Eh, jangan!",
		showLoaderOnConfirm: true,
		closeOnConfirm: false,
		closeOnCancel: false
	}, function (isConfirm) {
		if (isConfirm) {
			$.ajax({
				type: "POST",
				url: "/bem/action/del-user.php",
				data:'id='+val,
				success: function (data) {
					if (data.status == 'success') {
						swal("Deleted!", "Seluruh data pengguna telah dihapus.", "success");
						$('#dt-del-btn-b').click();
					} else {
						swal("Error!", "Terjadi galat, mohon refresh halaman dan coba lagi", "error");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					swal("Error!", "Terjadi galat, mohon refresh halaman dan coba lagi", "error");
				}
			});
		} else {
			swal("Cancelled", "Data pengguna aman :)", "error");
		}
	});
};

function admDel(val) {
	swal({
		title: "WARNING!",
		text: "Seluruh data admin ini akan dihapus termasuk pengguna, acara, posting dan peserta acara. Apakah Anda yakin?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya, hapus!",
		cancelButtonText: "Eh, jangan!",
		showLoaderOnConfirm: true,
		closeOnConfirm: false,
		closeOnCancel: false
	}, function (isConfirm) {
		if (isConfirm) {
			$.ajax({
				type: "POST",
				url: "/admin/action/del-admin.php",
				data:'id='+val,
				success: function (data) {
					if (data.status == 'success') {
						swal("Deleted!", "Seluruh data admin telah dihapus.", "success");
						$('#dt-del-btn-b').click();
					} else {
						swal("Error!", "Terjadi galat, mohon refresh halaman dan coba lagi", "error");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					swal("Error!", "Terjadi galat, mohon refresh halaman dan coba lagi", "error");
				}
			});
		} else {
			swal("Cancelled", "Data admin aman :)", "error");
		}
	});
};
