$(function () {
    $('.table-basic').DataTable({
        responsive: true,
        scrollX: true
    });
    var tableExB = $('.table-basic').DataTable();
    $('.table-basic tbody').on('click', '#dt-del-btn-b', function () {
        tableExB
            .row( $(this).parents('tr') )
            .remove()
            .draw();
    } );

    //Exportable table
    $('.table-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        scrollX: true
    });
    var tableEx = $('.table-exportable').DataTable();
    $('.table-exportable tbody').on('click', '#dt-del-btn', function () {
        tableEx
            .row( $(this).parents('tr') )
            .remove()
            .draw();
    } );

    //Exportable table
    $('.table-exportable-fixed').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        scrollX: true,
        fixedColumns:{
            leftColumns: 2
        }
    });
});
