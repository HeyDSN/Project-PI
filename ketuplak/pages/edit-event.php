<!-- Head -->
<?php include "head.php"; ?>
    
    <!-- Navigation BAR -->
    <?php include "navbar.php"; ?>

    <?php
    // Ambil data acara
    $query=$mysqli->prepare('SELECT * FROM `acara` WHERE `acara_id` = ?');
    $query->bind_param('s', $acaraID);
    $query->execute();
    $result=$query->get_result();
    while($row = $result->fetch_array()){
        $idAcara = $row['acara_id'];
        $namaAcara = $row['nama_acara'];
        $lokasiAcara = $row['lokasi_acara'];
        $tanggalAcara = $row['tgl_acara'];
        $waktuAcara = $row['waktu_acara'];
        $kuotaAcara = $row['kuota'];
        $statusAcara = $row['status'];
        $jenisAcara = $row['jenis'];
    }
    $query->close();

    // Pecah waktu acara
    $waktu = explode(" - ", $waktuAcara);
    $waktuAcaraMulai = $waktu[0];
    $waktuAcaraSelesai =  $waktu[1];
    ?>


    <section class="content">
        <div class="container-fluid">
            <?php if(isset($status)){
                if($status=="1"){ ?>
                    <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo $pesan; ?>
                    </div>
                    <?php 
                } 
                if($status=="2"){ ?>
                    <div class="alert bg-red alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo $pesan; ?>
                    </div>
                    <?php
                }
            }?>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>UBAH ACARA</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                <form id="form_validation" method="POST">
                                <input type="hidden" class="form-control" name="idAcara" value="<?php echo $idAcara; ?>" required />
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="namaAcara" value="<?php echo $namaAcara; ?>" required />
                                            <label class="form-label">Nama Acara</label>
                                        </div>
                                    </div>

                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="lokasiAcara" value="<?php echo $lokasiAcara; ?>" required />
                                            <label class="form-label">Lokasi Acara</label>
                                        </div>
                                    </div>

                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="datepicker form-control" name="tanggalAcara" value="<?php echo $tanggalAcara; ?>" required />
                                            <label class="form-label">Tanggal Acara</label>
                                        </div>
                                    </div>
                                    
                                    <div class="row clearfix">
                                    <div class="col-sm-6" style="margin-bottom: 0px !important;">
                                        <div class="form-group form-float">
                                            <div class="form-line focused">
                                                <input type="text" class="timepicker form-control" name="mulaiAcara" value="<?php echo $waktuAcaraMulai; ?>" required />
                                                <label class="form-label">Waktu Mulai</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" style="margin-bottom: 0px !important;">
                                        <div class="form-group form-float">
                                            <div class="form-line focused">
                                                <input type="text" class="timepicker form-control" name="selesaiAcara" value="<?php echo $waktuAcaraSelesai; ?>" required />
                                                <label class="form-label">Waktu Selesai</label>
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="hidden" name="kuotaAcaraBefore" value="<?php echo $kuotaAcara; ?>">
                                            <input type="number" min="1" class="form-control" name="kuotaAcara" data-toggle="tooltip" data-placement="left" value="<?php echo $kuotaAcara; ?>" data-original-title="Hanya isi dengan angka positif." required />
                                            <label class="form-label">Kuota Peserta</label>
                                            <div class="help-info">Isi dengan angka positif (Status acara akan berubah secara otomatis berdasarkan jumlah peserta dan kuota yang baru)</div>
                                        </div>
                                    </div>

                                    <div class="form-group form-float">
                                        <label>Jenis Acara</label>
                                        <select name="jenisAcara" class="form-control show-tick" required>
                                            <option value="" disabled>-- Please select --</option>
                                            <option value="0" <?php if($jenisAcara==0){echo "selected";} ?>>Khusus Mahasiswa Gunadarma</option>
                                            <option value="1" <?php if($jenisAcara==1){echo "selected";} ?>>Umum</option>
                                        </select>
                                    </div>

                                    

                                    <div class="form-group">
                                        <input type="submit" name="editAcara" class="btn btn-block btn-primary m-t-15 waves-effect" />
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->

        </div>
    </section>
    
    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/dash/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="/dash/plugins/jquery-datatable/jquery.dataTables.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="/dash/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="/dash/plugins/jquery-validation/localization/messages_id.js"></script>

    <!-- Select Plugin Js -->
    <script src="/dash/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/dash/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="/dash/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="/dash/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/form-validation.js"></script>
    <script src="/dash/js/jquery-datatable.js"></script>
    <script src="/dash/js/basic-form-elements.js"></script>
    <script src="/dash/js/tooltips-popovers.js"></script>
</body>

</html>
