<?php
// sertakan berkas utama
$role = "user";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';


// Cek status login
if(empty($_SESSION['UIDUser'])){
    header("HTTP/1.1 404 Not Found");
    die();
}

// Get hash
$query=$mysqli->prepare('SELECT `email_hash`,`ganti_email` FROM `pengguna` WHERE pengguna_id = ?');
$query->bind_param('s', $userID);
$query->execute();
$result=$query->get_result();
while($row = $result->fetch_array()){
    $mailHash = $row['email_hash'];
    $newEmail = $row['ganti_email'];
}
$query->close();

$newNama = $userNama;

include 'chmail.mail.php';
$_SESSION['notice'] = "Link verifikasi berhasil dikirim ulang, Anda akan menerima email dalam beberapa manit.";
header('Location: /ketuplak/profile/');
?>
