<?php
// sertakan berkas utama
$role = "user";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

if(isset($_GET['id'],$_GET['token'])){
    $id = $_GET['id'];
    $token = $_GET['token'];
    // Count
	$query = $mysqli->prepare('SELECT id,ganti_email FROM pengguna WHERE pengguna_id = ? AND email_hash = ?');
    $query->bind_param('ss', $id, $token);
    $query->execute();

    $result=$query->get_result();
    $jumlahBaris = $result->num_rows;

	if($jumlahBaris == 1){
		while($row = $result->fetch_array()){
			$id = $row['id'];
            $newMail = $row['ganti_email'];
		}
		$query->close();

		$email_hash = "Activated";
		$empty = "";
		$query = $mysqli->prepare('UPDATE pengguna SET email = ?, email_hash = ?, ganti_email = ? WHERE id = ?');
    	$query->bind_param('ssss', $newMail, $email_hash, $empty, $id);
    	$query->execute();
        $query->close();
        $actStat = true;
    }else{
        $actStat = false;
    }
}else{
    $actStat = false;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Portal | Acara Online</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/dash/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/dash/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="/dash/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/dash/css/style.css" rel="stylesheet">
</head>

<body class="signup-page custom-bg" style="max-width: 600px;">
<?php if($actStat == true){ ?>
    <div class="signup-box">
        <div class="logo">
            <a href="#">Change Email</a>
            <small>Chame Email Success</small>
        </div>
        <div class="card">
            <div class="body" style="display: block; padding: 25px 30px; background: rgba(255,255,255,.9); font: 300 18px/27px 'Open Sans', Helvetica, Arial, sans-serif; text-align: center;">
                <p align="center">
                    <i class="fa fa-check aria-hidden fa-5x" style="color: green;"></i><br/><br/>
                    Email baru Anda telah berhasil di verifikasi, email berhasil diganti.<br/><br/>
                    <a href="/user/" style="font-size: 24px;">Login</a>
                </p>
            </div>
        </div>
    </div>
<?php } ?>

<?php if($actStat == false){ ?>
    <div class="signup-box">
        <div class="logo">
            <a href="#">Oops!</a>
            <small>There's nothing here.</small>
        </div>
        <div class="card">
            <div class="body" style="display: block; padding: 25px 30px; background: rgba(255,255,255,.9); font: 300 18px/27px 'Open Sans', Helvetica, Arial, sans-serif; text-align: center; color: #f00;">
                <p>
                    <i class="fa fa-times aria-hidden fa-5x"></i><br/>
                    Whatever you were looking for doesn't currently exist at this address. Unless you were looking for this error page, in which case: Congrats! You totally found it. 
                </p>
            </div>
        </div>
    </div>
<?php } ?>

    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="/dash/plugins/sweetalert/sweetalert.min.js"></script>
    <?php if($actStat == true){ echo 
    '<script>
        function mailAct() {
            swal("Mail Changed", "Selamat, email Anda berhasil diubah!", "success");
        }
    </script>
    <script>mailAct();</script>'; 
    }; ?>
</body>

</html>
