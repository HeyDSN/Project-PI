﻿<?php
// sertakan berkas utama
$role = "user";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

// Cek status login
if(empty($_SESSION['UIDUser'])){
    if($enableCaptcha==false){
        include 'pages/login.php';
    }else if($enableCaptcha==true){
        include 'pages/login-captcha.php';
    }
die();
}

// Set page
if(!isset($_GET['page'])){
    $page = "";
}else{
    $page = $_GET['page'];
}

if($userStatus == "0"){
    $pageTitle = "Activating | Panel Ketuplak";
    include 'activate-user.php';
    exit;
}

// Halaman utama
if($page == "dashboard"){
	$pageTitle = "Dashboard | Panel Ketuplak";
    include 'pages/dashboard.php';

// Halaman acara dengan aksi
}else if($page == "events"){
    if(!isset($_GET['x'])){header('Location: /ketuplak/events/view/');}
    
    // View acara
    if($_GET['x']=="view"){
        $pageTitle = "Daftar Acara | Panel Ketuplak";
    
        // Tambah acara
        if(isset($_POST['newAcara'])){
            include 'action/new-event.php';
        }
        include 'pages/events.php';
        die;
    }
    
    // Add
    if($_GET['x']=="add"){
        $x = "add";
        $pageTitle = "Tambah Acara | Panel Ketuplak";
        include 'pages/events.php';
        die;
    }

    $acaraID=$_GET['y'];
    // Redirect
    $db_handle = new DBController();
    $query ="SELECT * FROM `acara` WHERE `acara_id` = '$acaraID'";
    $results = $db_handle->countQuery($query);
    if($results == 0){
        header('Location: /ketuplak/events/view/');
    }

    // Edit
    if($_GET['x']=="edit"){
        $pageTitle = "Edit Acara | Panel Ketuplak";
        if(isset($_POST['editAcara'])){
            include 'action/edit-acara.php';
        }
        include 'pages/edit-event.php';
    }

    // Close
    if($_GET['x']=="close"){
        include 'action/tutup-acara.php';
        header("Location: /ketuplak/events/view/?s=$status&mess=".urlencode($pesan));
    }

    // Open
    if($_GET['x']=="open"){
        include 'action/buka-acara.php';
        header("Location: /ketuplak/events/view/?s=$status&mess=".urlencode($pesan));
    }

// Halaman peserta
}else if($page == "participants"){
    $pageTitle = "Daftar Peserta | Panel Ketuplak";
    $acaraID = $_GET['x'];
    // Cek jika data seminar valid
    if($acaraID != 'all'){
        $db_handle = new DBController();
        $query ="SELECT id FROM `acara` WHERE `acara_id` = '$acaraID'";
        $results = $db_handle->countQuery($query);
        if($results == 0){
            header('Location: /ketuplak/participants/all');
        }
    }
    include 'pages/participants.php';

// Halaman peserta aktif
}else if($page == "active-participants"){
    $pageTitle = "Daftar Peserta Aktif | Panel Ketuplak";
    $acaraID = $_GET['x'];
    // Cek jika data seminar valid
    $db_handle = new DBController();
    $query ="SELECT id FROM `acara` WHERE `acara_id` = '$acaraID'";
    $results = $db_handle->countQuery($query);
    if($results == 0){
        header('Location: /ketuplak/dashboard');
    }
    include 'pages/active-participants.php';

// Halaman profile admin
}else if($page == "profile"){
	$pageTitle = "Profile | Panel Ketuplak";

	// Edit biodata
	if(isset($_POST['updateUser'])){
		include 'action/edit-biodata.php';
	}

    // Ganti Password
	if(isset($_POST['chpasswordUser'])){
		include 'action/edit-password.php';
	}

    include "pages/profile.php";

// Halaman posting
}else if($page == "posting"){
    if(!isset($_GET['x'])){header('Location: /ketuplak/posting/view/');}
    
    // View acara
    if($_GET['x']=="view"){
        $pageTitle = "Daftar Posting | Panel Ketuplak";
        include 'pages/posting.php';
        die;
    }

    if($_GET['x']=="edit"){
        $pageTitle = "Edit Posting | Panel Ketuplak";
        if(isset($_POST['editPosting'])){
            include 'action/edit-posting.php';
        }
        include 'pages/posting.php';
        die;
    }
    
}else if($page == ""){
    header('Location: /ketuplak/dashboard');
}else{
    header('Location: /ketuplak/dashboard');
}
?>
