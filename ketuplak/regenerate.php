<?php
// sertakan berkas utama
$role = "user";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';


// Cek status login
if(empty($_SESSION['UIDUser'])){
    header("HTTP/1.1 404 Not Found");
    die();
}

// Get hash
$query=$mysqli->prepare('SELECT `email_hash` FROM `pengguna` WHERE pengguna_id = ?');
$query->bind_param('s', $userID);
$query->execute();
$result=$query->get_result();
while($row = $result->fetch_array()){
    $mail_hash = $row['email_hash'];
}
$query->close();

include 'hash.mail.php';

header('Location: /ketuplak/dashboard/?regenerate=sent')
?>
