<?php
// Cek status login
if(empty($_SESSION['UIDUser'])){
header("HTTP/1.1 404 Not Found");
die();
}
    $pwLama = $_POST['oldPw'];
    $pwBaru = $_POST['newPw'];

    $query=$mysqli->prepare('SELECT hash FROM pengguna WHERE pengguna_id = ?');
    $query->bind_param('s', $userID);
    $query->execute();

    $result = $query->get_result();
    while($row = $result->fetch_array()){
        $hash = $row['hash'];
    }
    $query->close();

    if(password_verify($pwLama, $hash)){
        // Always reset token for secutory
        $mail_hash = "Activated";
        $password = $pwBaru;
        $options = [
        'cost' => 10
        ];
        $newHash = password_hash($password, PASSWORD_BCRYPT, $options);
        $query = $mysqli->prepare('UPDATE `pengguna` SET hash = ?, email_hash = ? WHERE pengguna_id = ?');
        $query->bind_param('sss', $newHash, $mail_hash, $userID);
        $query->execute();
        $query->close();
        $chStatus = true;
    }else{
        $chStatus = false;
    }
?>
