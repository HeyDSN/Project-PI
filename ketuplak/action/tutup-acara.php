<?php
// Cek status login
if(empty($_SESSION['UIDUser'])){
header("HTTP/1.1 404 Not Found");
die();
}
    // Ambil data acara
    $query=$mysqli->prepare('UPDATE `acara` SET status = "2" WHERE `acara_id` = ? AND `pengguna_id` = ?');
    $query->bind_param('ss', $acaraID, $userID);
    $query->execute();
    if($query->execute()){
        $status="1";
        $pesan="Status acara berhasil diubah.";
    }else{
        $status="2";
        $pesan="Status acara gagal diubah.";
    }
    $query->close();
?>
