<?php
// sertakan berkas utama
$role = "user";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

// Cek status login
if(empty($_SESSION['UIDUser'])){
header("HTTP/1.1 404 Not Found");
die();
}
    // String
    $idPost = $_POST['id'];

    // Delete post
    $query=$mysqli->prepare("DELETE FROM posting WHERE posting_id=? AND pengguna_id = ?");
    $query->bind_param('ss', $idPost, $userID);

    if ($query->execute()) { 
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'failed';
    }
    $query->close();
    header('Content-type: application/json');
    echo json_encode($response_array);
?>
