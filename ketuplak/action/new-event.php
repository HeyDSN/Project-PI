<?php
// Cek status login
if(empty($_SESSION['UIDUser'])){
header("HTTP/1.1 404 Not Found");
die();
}
    $idAcara = randomKey();
    $namaAcara = $_POST['namaAcara'];
    $lokasiAcara = $_POST['lokasiAcara'];
    $tanggalAcara = $_POST['tanggalAcara'];
    $jenisAcara = $_POST['jenisAcara'];

    // Waktu acara
    $waktuAcara = $_POST['mulaiAcara']." - ".$_POST['selesaiAcara'];

    $kuotaAcara = $_POST['kuotaAcara'];
    $userID = $_SESSION['UIDUser'];

    $query=$mysqli->prepare('SELECT admin_id FROM pengguna WHERE pengguna_id = ?');
    $query->bind_param('s', $userID);
    $query->execute();
    $result=$query->get_result();
    while($row = $result->fetch_array()){
        $adminID = $row['admin_id'];
    }

    if(isset($_POST['postAcara'])){
        $idPost = randomKey();
        $judulPost = $namaAcara;
        $isiPost = $_POST['isiPost'];
        $headerPost = $_POST['headerPost'];
        
        // Waktu posting
        $date = date("Y-m-d");
        $tanggal = formatIndonesia($date, true);
        $waktu = date("H:i:s");
        $waktuPosting = $tanggal." | ".$waktu;

        // Input acara
        $query = $mysqli->prepare('INSERT INTO acara (acara_id, nama_acara, lokasi_acara, tgl_acara, waktu_acara, kuota, jenis, pengguna_id)values(?, ?, ?, ?, ?, ?, ?, ?)');
        $query->bind_param('ssssssss', $idAcara, $namaAcara, $lokasiAcara, $tanggalAcara, $waktuAcara, $kuotaAcara, $jenisAcara, $userID);
        $query->execute();
        $query->close();

        // Input posting
        $query = $mysqli->prepare('INSERT INTO posting (posting_id, judul, waktu, isi, header, admin_id, pengguna_id, acara_id)values(?, ?, ?, ?, ?, ?, ?, ?)');
        $query->bind_param('ssssssss', $idPost, $judulPost, $waktuPosting, $isiPost, $headerPost, $adminID, $userID, $idAcara);
        $query->execute();
        $query->close();

        // Send status sukses
        $regStatus = "sukses";
        $regAcara = $namaAcara;
    }else{
        // Hanya input acara ke table
        $query = $mysqli->prepare('INSERT INTO acara (acara_id, nama_acara, lokasi_acara, tgl_acara, waktu_acara, kuota, jenis, pengguna_id)values(?, ?, ?, ?, ?, ?, ?, ?)');
        $query->bind_param('ssssssss', $idAcara, $namaAcara, $lokasiAcara, $tanggalAcara, $waktuAcara, $kuotaAcara, $jenisAcara, $userID);
        $query->execute();
        $query->close();

        // Send status sukses
        $regStatus = "sukses";
        $regAcara = $namaAcara;
    }
?>
