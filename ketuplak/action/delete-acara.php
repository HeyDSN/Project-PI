<?php
// sertakan berkas utama
$role = "user";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

// Cek status login
if(empty($_SESSION['UIDUser'])){
header("HTTP/1.1 404 Not Found");
die();
}
    // String
    $idAcara = $_POST['id'];
    $status = 0;

    // Delete peserta
    $query=$mysqli->prepare("DELETE FROM peserta WHERE acara_id=? AND pengguna_id = ?");
    $query->bind_param('ss', $idAcara, $userID);
    if($query->execute()){
        $status = $status + 1;
    }
    $query->close();

    // Delete acara
    $query=$mysqli->prepare("DELETE FROM acara WHERE acara_id=? AND pengguna_id = ?");
    $query->bind_param('ss', $idAcara, $userID);
    if($query->execute()){
        $status = $status + 1;
    }
    $query->close();

    if ($status == 2) { 
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'failed';
    }
    header('Content-type: application/json');
    echo json_encode($response_array);
?>
