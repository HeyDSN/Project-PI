<?php
// sertakan berkas utama
$role = "none";
require_once $_SERVER['DOCUMENT_ROOT'].'/include/load.php';

$db_handle = new DBController();
if(!empty($_POST["q"])) {
	$npm=$_POST['q'];
	$acara_id=$_POST['z'];
	$query ="SELECT peserta.peserta_id,peserta.npm,peserta.nama,peserta.email,acara.nama_acara,acara.jenis FROM peserta INNER JOIN acara ON acara.acara_id=peserta.acara_id WHERE peserta.npm='$npm' AND peserta.acara_id='$acara_id'";
	$results = $db_handle->selectQuery($query);
	if($results==0){
		?>
		<div class="alert bg-orange" role="alert">
            NPM tidak ditemukan.
        </div>
		<p align="justify">NPM Anda tidak ditemukan pada accara yang dipilih, sepertinya Anda belum terdaftar di acara tersebut atau terjadi sebuah kesalahan.<br/><br/>
		Harap cek kembali NPM / No. Identitas yang Anda masukkan dan pastikan tidak terjadi kesalahan mengetik, serta pastikan juga Anda memilih acara yang sesuai dengan pendaftaran Anda, lalu coba lagi dengan cara klik tombol di bawah.<br/><br/>
		Jika Anda yakin telah memilih acara yang benar dan memasukkan NPM / No. Identitas dengan benar silahkan hubungi panitia untuk informasi selanjutnya.</p>
		<button class="btn btn-primary btn-block" onclick="tryAgain()">Try Again</button>
		<?php
	}else{
		foreach($results as $row) {
			$peserta_id = $row['peserta_id'];
			$nama_acara = $row['nama_acara'];
			$jenis_acara = $row['jenis'];
			$npm = $row['npm'];
			$nama = $row['nama'];
			$email = $row['email'];
		}
		?>
		<form id="finalProfile" action="/portal/profile/" method="post">
			<div class="msg">Silahkan lengkapi data Anda.</div>
			<div class="form-group">
			    <div class="form-line">
			    <h2 class="card-inside-title">Acara</h2>
					<input type="hidden" name="lengkapiProfile" value="send" />
			        <input type="hidden" name="kodeAcara" value="<?php echo $acara_id ?>" />
			        <input type="text" class="form-control" name="namaAcara" value="<?php echo $nama_acara ?>" readonly required />
			    </div>
			</div>

			<div class="form-group">
			    <div class="form-line">
			    <h2 class="card-inside-title">NPM / No. Identitas</h2>
					<input type="hidden" name="peserta_id" value="<?php echo $peserta_id ?>" />
			        <input type="text" class="form-control" name="npm" value="<?php echo $npm ?>" readonly required />
			    </div>
			</div>

			<div class="form-group">
			    <div class="form-line">
			    <h2 class="card-inside-title">Nama</h2>
			        <input type="text" class="form-control" name="nama" value="<?php echo $nama ?>" readonly required />
			    </div>
			</div>

			<div class="form-group">
			    <div class="form-line">
			    <h2 class="card-inside-title">Email</h2>
			        <input type="text" class="form-control" name="email" value="<?php echo $email ?>" readonly required />
			    </div>
			</div>

			<div class="form-group">
			    <div class="form-line">
			    <h2 class="card-inside-title">No. Handphone</h2>
			        <input id="hp" type="text" class="form-control" name="hp" placeholder="No. Handphone" required />
			    </div>
			</div>

			<div class="form-group form-float">
			    <div class="form-line">
			    <h2 class="card-inside-title">Tanggal Lahir</h2>
			        <input data-provide="datepicker" type="text" class="form-control" name="tglLhr" id="datepicker" placeholder="Tanggal Lahir Anda" autocomplete="off" required />
			    </div>
			</div>

			<div class="form-group form-float">
			    <div class="form-line">
			    <h2 class="card-inside-title"><?php if($jenis_acara==0){echo "Jurusan";}else{echo "Alamat";} ?></h2>
			        <input type="text" class="form-control" name="varOne" placeholder="<?php if($jenis_acara==0){echo "Jurusan";}else{echo "Alamat";} ?>" required />
			    </div>
			</div>

			<div class="form-group form-float">
			    <div class="form-line">
			    <h2 class="card-inside-title"><?php if($jenis_acara==0){echo "Kelas";}else{echo "Pekerjaan";} ?></h2>
			        <input type="text" class="form-control" name="varTwo" placeholder="<?php if($jenis_acara==0){echo "Kelas";}else{echo "Pekerjaan";} ?>" required />
			    </div>
			</div>

			<div class="form-group form-float">
			    <div class="form-line">
			    <h2 class="card-inside-title">Domisili</h2>
			        <select class="form-control" name="domisili" id="domisili" required="">
				        <option disabled selected>-- Domisili Anda --</option>
				        <option value="Depok">Depok</option>
				        <option value="Kalimalang">Kalimalang</option>
				        <option value="Karawaci">Karawaci</option>
				        <option value="Salemba">Salemba</option>
				        <option value="Lainnya">Lainnya</option>
				    </select>
			    </div>
			</div>

			<div class="input-group">
			    <input class="btn btn-block btn-lg bg-blue waves-effect" type="submit" name="finalProfile" value="Kirim Data" onclick="allProfile();">
			</div>
		</form>
		<script>
		// store original so we can call it inside our overriding method
		$.datepicker._generateMonthYearHeader_original = $.datepicker._generateMonthYearHeader;

		$.datepicker._generateMonthYearHeader = function(inst, dm, dy, mnd, mxd, s, mn, mns) {
		  var header = $($.datepicker._generateMonthYearHeader_original(inst, dm, dy, mnd, mxd, s, mn, mns)),
		      years = header.find('.ui-datepicker-year');

		  // reverse the years
		  years.html(Array.prototype.reverse.apply(years.children()));

		  // return our new html
		  return $('<div />').append(header).html();
		}

		$(function(){
			$("#datepicker").datepicker({
				yearRange: "-80:-10",
				dateFormat: "dd-mm-yy",
				changeMonth: true,
				changeYear: true
			});
		});
		</script>
		<?php
	}
}
?>
