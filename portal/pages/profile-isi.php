<?php
$acara_id = $_GET['z'];
$db_handle = new DBController();
$query ="SELECT acara_id,nama_acara FROM acara WHERE acara_id = '$acara_id'";
$results = $db_handle->selectQuery($query);
foreach($results as $acara) {
    $acara_id=$acara['acara_id'];
    $nama_acara=$acara['nama_acara'];
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Portal | Acara Online</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/dash/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/dash/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/dash/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="/dash/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Jquery UI Css-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Sweetalert Css -->
    <link href="/dash/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/dash/css/style.css" rel="stylesheet">
</head>

<body class="signup-page custom-bg" style="max-width: 600px;">
<div class="modal"></div>
    <div class="signup-box">
        <div class="logo">
            <a href="/portal/">Portal</a>
            <small>Selamat datang di portal Acara Online Gunadarma</small>
        </div>
        <div class="card">
            <div class="body">

                <form id="validateNPM" method="post">
                    <div class="msg">Silahkan lengkapi data Anda.</div>
                    <div class="form-group form-float" style="margin-bottom: 0px; margin-top: 5px;">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="form-line">
                                    <input id="checkAcara" type="hidden" class="form-control" name="checkAcara" value="<?php echo $_GET['z']; ?>" required />
                                    <input id="checkNPM" type="text" class="form-control" name="checkNPM" required />
                                    <label class="form-label">NPM / No. Identitas</label>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-line">
                                    <button type="submit" class="btn btn-primary waves-effect pull-right" onclick="validateNPMF();"><i class="material-icons">search</i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div id="fillProfile"></div>

                <div class="sukses" style="display:none;">
                    <div class="alert bg-green" role="alert">
                        Data berhasil di kirim
                    </div>
                    <p align="center">
                        <i class="fa fa-check aria-hidden fa-5x"></i><br/>
                        Anda telah berhasil melengkapi profile, silahkan cek email untuk mengunduh tiket, jika tiket tidak ada pada folder 'kotak masuk'/'inbox' pastikan cek juga di folder 'spam' email Anda, jika dalam 30 menit Anda tidak menerima tiket pada email, silahkan coba mengisi profile kembali atau hubungi panitia dan datang ke stand pendaftaran untuk informasi lebih lanjut.
                    </p>
                </div>

            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="/dash/plugins/jquery/jquery.min.js"></script>

    <!-- Jquery UI Js -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/dash/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/dash/plugins/node-waves/waves.js"></script>

    <!-- Select Plugin Js -->
    <script src="/dash/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Validation Plugin Js -->
    <script src="/dash/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="/dash/plugins/sweetalert/sweetalert.min.js"></script>
    
    <!-- Custom Js -->
    <script src="/dash/js/admin.js"></script>
    <script src="/dash/js/ajax-func.js"></script>
    <script src="/dash/js/dialogs.js"></script>
</body>

</html>
