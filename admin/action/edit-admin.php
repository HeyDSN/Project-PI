<?php
// Cek status login
if(empty($_SESSION['UIDSuperAdmin'])){
    header("HTTP/1.1 404 Not Found");
    die();
}

$admin_id = $_POST['adminID'];
$nama = $_POST['nama'];
$email = $_POST['email'];

$query=$mysqli->prepare('SELECT email FROM admin WHERE admin_id = ?');
$query->bind_param('s', $admin_id);
$query->execute();
$result=$query->get_result();
while($row = $result->fetch_array()){
    $emailGet = $row['email'];
}

if($emailGet != $email){
    $db_handle = new DBController();
    $query ="SELECT COUNT(id) FROM `admin` WHERE ganti_email = '$email' OR email = '$email'";
    $validate_email = $db_handle->countQuery($query);
    if($validate_email > 0){
        $status = "fail";
    }else{
        $adminChMail=true;
        $time = date('Y-m-d');
        $mailHash = hash('sha512', $time.$email.'kopiadem');
        $query = $mysqli->prepare('UPDATE `admin` SET nama = ?, email_hash = ?, ganti_email = ? WHERE admin_id = ?');
        $query->bind_param('ssss', $nama, $mailHash, $email, $admin_id);
        $query->execute();
        $status = "Sukses";
        $query->close();
        include "chmail.mail.php";
    }
}else{
    $adminChMail=false;
    $query = $mysqli->prepare('UPDATE `admin` SET nama = ? WHERE admin_id = ?');
    $query->bind_param('ss', $nama, $admin_id);
    $query->execute();
    $status = "Sukses";
    $query->close();
};

if($status == "Sukses"){
    $statusGanti = 1;
}else{
    $statusGanti = 0;
}
?>
