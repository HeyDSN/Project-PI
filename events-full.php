<!DOCTYPE html>
<html lang="en-US">
  <head>

    <!-- Bagian META -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $judul; ?> - UG BEMF Events</title>
    <meta name="description" content="Gunadarma awesome events info on one place."/>

    <!-- Bagian STYLE -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/materialize/css/material.indigo-pink.min.css" rel="stylesheet">
    <link href="/materialize/css/main.css" rel="stylesheet">
    
    <!-- Search -->
    <link rel="stylesheet" type="text/css" href="/materialize/css/default.css" />
    <link rel="stylesheet" type="text/css" href="/materialize/css/component.css" />
    <script src="/materialize/js/modernizr.custom.js"></script>
    <!-- Search END -->
  </head>

  <body id="top">
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <a href="contact.php" id="contact-button" class="mdl-button mdl-button--fab mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast mdl-shadow--4dp"><i class="material-icons">mail</i></a>

        <div style="width:240px;" id="search-button">
          <div id="sb-search" class="sb-search">
            <form action="events.php" method="GET">
              <input class="sb-search-input" placeholder="Tekan (Enter)..." type="text" value="" name="q" id="search">
              <input class="sb-search-submit" type="submit" value="">
              <span class="sb-icon-search"></span>
            </form>
          </div>
        </div>

  <header class="mdl-layout__header mdl-layout__header--waterfall site-header" style="overflow:visible;">
    <div class="mdl-layout__header-row site-logo-row">
      <span class="mdl-layout__title">
        <div class="site-logo"></div>
      </span>
    </div>
    
    <div class="mdl-layout__header-row site-navigation-row mdl-layout--large-screen-only">
      <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
        <a class="mdl-navigation__link" href="./">Home</a>
        <a class="mdl-navigation__link" href="events.php">Acara</a>
        <a class="mdl-navigation__link" href="about.php">Tentang</a>
        <a class="mdl-navigation__link" href="contact.php">Kontak</a>
        <a class="mdl-navigation__link" href="/portal/">Portal</a>
        <div class="mdl-navigation__link dropdown">
            <a class="mdl-navigation__link" href="javascript:void(0);" style="padding:0px;">Login</a>
            <div class="dropdown-content">
                <a href="/admin/">Admin</a>
                <a href="/bem/">BEM</a>
                <a href="/ketuplak/">Ketuplak</a>
            </div>
        </div>
      </nav>
    </div>
  </header>

  <div class="mdl-layout__drawer mdl-layout--small-screen-only">
      <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
          <a class="mdl-navigation__link" href="./">Home</a>
          <a class="mdl-navigation__link" href="events.php">Acara</a>
          <a class="mdl-navigation__link" href="about.php">Tentang</a>
          <a class="mdl-navigation__link" href="contact.php">Kontak</a>
          <a class="mdl-navigation__link" href="/portal/">Portal</a>
          <a class="mdl-navigation__link" href="/admin/">Login Admin</a>
          <a class="mdl-navigation__link" href="/bem/">Login BEM</a>
          <a class="mdl-navigation__link" href="/ketuplak/">Login Ketuplak</a>
      </nav>
  </div>
        
      <main class="mdl-layout__content">
        <div class="site-content">
          <div class="mdl-grid site-max-width">
            <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--4dp page-content">
              <div class="mdl-card__title">
                <h1 class="mdl-card__title-text"><?php echo $judul ?></h1>
              </div>
              <div class="mdl-card__media">
                <img class="article-image-full" src="<?php echo $header; ?>" border="0" alt="Portfolio Page">
              </div>
              <div class="mdl-grid site-copy">
                <div class="mdl-cell mdl-cell--12-col">
                  <p><h3 class="mdl-cell mdl-cell--12-col mdl-typography--headline">Events Info</h3></p>
                    <div class="mdl-cell mdl-cell--10-col mdl-card__supporting-text no-padding ">
                      <?php echo $isi; ?>
                      <br/><br/>
                      <p><small>Posted on: <?php echo $waktu; ?></small></p>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <footer class="mdl-mini-footer">
                <div class="footer-container">
                    <div class="mdl-logo">&copy; 2017 Kopiadem. Back to <a href="/">HOME</a></div>
                </div>
            </footer>
      </main>
      <script src="https://code.getmdl.io/1.3.0/material.min.js" defer></script>

      <!-- Search -->
      <script src="/materialize/js/classie.js"></script>
      <script src="/materialize/js/uisearch.js"></script>
      <script>
        new UISearch( document.getElementById( 'sb-search' ) );
      </script>
    </div>
  </body>
</html>
