<?php
// Settings
$array = parse_ini_file("config.ini", true);
$config = json_decode(json_encode($array), FALSE);

//echo $config->mail->host;

// Database
require_once $_SERVER['DOCUMENT_ROOT'].'/include/db.php';

// Function
require_once $_SERVER['DOCUMENT_ROOT'].'/include/func.php';

// Authentication
if(isset($role)){
    if($role == "superadmin"){
        require_once $_SERVER['DOCUMENT_ROOT'].'/include/authvSuperAdmin.php';
    }else if($role == "admin"){
        require_once $_SERVER['DOCUMENT_ROOT'].'/include/authvAdmin.php';
    }else if($role == "user"){
        require_once $_SERVER['DOCUMENT_ROOT'].'/include/authvUser.php';
    }else{
        // No auth included
    }
}else{
    echo "Who are you?";
    die();
}

// Config
require_once $_SERVER['DOCUMENT_ROOT'].'/include/config.php';
?>
