<?php
session_start();

$limit=$config->login_limit->superadmin;

//if logout trigger called
if(isset($_GET['logout'])) {
    $_SESSION['NamaSuperAdmin'] = '';
	$_SESSION['UIDSuperAdmin'] = '';
	session_destroy();
	header('Location:  ' . $_SERVER['PHP_SELF']);
}

//something interesting, just playing with script :3
if(!isset($_SESSION['failSuperAdmin'])){
	$no = 0;
	$_SESSION['failSuperAdmin'] = 0;
}else{
	$no=$_SESSION['failSuperAdmin'];
}
$no++;

if($_SESSION['failSuperAdmin'] >= $limit){
	$enableCaptchaSuperAdmin = true;
}else{
	$enableCaptchaSuperAdmin = false;
}

//mulai script login	
if(isset($_POST['submit'])) {
	//cek username di isi
	if(empty($_POST['username'])){
		$_SESSION['failSuperAdmin']= $no; //adding fail every failed attempt
		$_SESSION['failSuperAdmin']= $no; //adding fail every failed attempt
	};
	//cek password di isi
	if(empty($_POST['password'])){
		$_SESSION['failSuperAdmin']= $no; //adding fail every failed attempt
		$_SESSION['failSuperAdmin']= $no; //adding fail every failed attempt
	};
}

// Cek terakhir

// Tanpa captcha
if($enableCaptchaSuperAdmin==false){
	if(isset($_POST['password']) and isset($_POST['password'])){
		$username = $_POST['username'];
		$password = $_POST['password'];

		$query=$mysqli->prepare('SELECT super_id,hash FROM superadmin WHERE username=?');
		$query->bind_param('s', $username);
		$query->execute();

		$result = $query->get_result();
		while($row = $result->fetch_array()){
			$uid = $row['super_id'];
			$hash = $row['hash'];
		}

		$row = $result->num_rows;

		$query->close();

		if($row == 1){
			if(password_verify($password, $hash)){
				$loginStatusSuperAdmin = true;
			}else{
				$loginStatusSuperAdmin = false;
			}
		}else{
			$loginStatusSuperAdmin = false;
		}

		if($loginStatusSuperAdmin == true){
			$_SESSION['failSuperAdmin']= '';
			$_SESSION['NamaSuperAdmin'] = $username;
			$_SESSION['UIDSuperAdmin'] = $uid;
		}else{
			$_SESSION['failSuperAdmin']= $no; //adding fail every failed attempt
			header("Location: /admin/?fail");
		}
	}
// Dengan captcha
}else{
	if(isset($_POST['g-recaptcha-response'])){
		$captcha=$_POST['g-recaptcha-response'];
        if(!$captcha){
        	echo '<script>alert("Please check the the captcha form!");</script>';
    	}else{
			$key = $config->api_key->googleCaptchaSecret;
			$serv = $_SERVER['REMOTE_ADDR'];
    		$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$key&response=$captcha&remoteip=$serv"), true);
    		if($response['success'] == false){
        		$_SESSION['failSuperAdmin']= $no; //adding fail every failed attempt
				echo '<script>alert("Detected as Bot!");</script>';
				echo '<script>alert("Try again!");</script>';
    		}else{
        		if(isset($_POST['password'])){
					$username = $_POST['username'];
					$password = $_POST['password'];

					$query=$mysqli->prepare('SELECT super_id,hash FROM superadmin WHERE username=?');
					$query->bind_param('s', $username);
					$query->execute();
					$result = $query->get_result();
					
					while($row = $result->fetch_array()){
						$uid = $row['super_id'];
						$hash = $row['hash'];
					}

					$row = $result->num_rows;

					$query->close();

					if($row == 1){
						if(password_verify($password, $hash)){
							$loginStatusSuperAdmin = true;
						}else{
							$loginStatusSuperAdmin = false;
						}
					}else{
						$loginStatusSuperAdmin = false;
					}

					if($loginStatusSuperAdmin == true){
						$_SESSION['failSuperAdmin']= '';
						$_SESSION['NamaSuperAdmin'] = $username;
						$_SESSION['UIDSuperAdmin'] = $uid;
					}else{
						$_SESSION['failSuperAdmin']= $no; //adding fail every failed attempt
						header("Location: /admin/?fail");
					}
				}
    		}
    	}
	}
}
?>
