<?php
date_default_timezone_set('Asia/Jakarta');
$hostDB = $config->db->host;
$nameDB = $config->db->name;
$userDB = $config->db->user;
$passDB = $config->db->pass;

$mysqli = new mysqli($hostDB, $userDB, $passDB, $nameDB);

if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    die();
}
?>
